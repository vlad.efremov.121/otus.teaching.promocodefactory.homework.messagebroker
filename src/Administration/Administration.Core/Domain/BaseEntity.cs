﻿using System;

namespace Administration.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}