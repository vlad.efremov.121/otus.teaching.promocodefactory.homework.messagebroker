﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Administration.Core.Abstractions.Services
{
    public interface IEmployeeService
    {
        Task UpdateAppliedPromocodesAsync(Guid id);
    }
}
