﻿using Administration.Core.Abstractions.Services;
using CommonNamespace;
using MassTransit;
using System.Threading.Tasks;

namespace Administration.Core.Consumers
{
    public class GivePromoCodeToCustomerConsumer : IConsumer<GivePromoCodeToCustomerDto>
    {
        private readonly IEmployeeService _employeeService;

        public GivePromoCodeToCustomerConsumer(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerDto> context)
        {
            var request = context.Message;

            if (request.PartnerManagerId.HasValue)
            {
                await _employeeService.UpdateAppliedPromocodesAsync(request.PartnerManagerId.Value);
            }
        }

    }
}
