﻿using CommonNamespace;
using GivingToCustomer.Core.Abstractions.Services;
using MassTransit;
using System.Threading.Tasks;

namespace Administration.Core.Consumers
{
    public class GivePromoCodeToCustomerConsumer : IConsumer<GivePromoCodeToCustomerDto>
    {
        private readonly IPromoCodeService _promoCodeService;

        public GivePromoCodeToCustomerConsumer(IPromoCodeService promoCodeService)
        {
            _promoCodeService = promoCodeService;
        }
        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerDto> context)
        {
            var request = context.Message;

            await _promoCodeService.SavePromoCode(request);
        }
    }

}
