﻿using System;
using System.Collections.Generic;
using CommonNamespace;
using GivingToCustomer.Core.Domain;

namespace GivingToCustomer.WebHost.Mappers
{
    public class PromoCodeMapper
    {
        public static PromoCode MapFromModel(GivePromoCodeToCustomerDto dto, Preference preference, IEnumerable<Customer> customers) 
        {

            var promocode = new PromoCode();
            promocode.Id = dto.PromoCodeId;
            
            promocode.PartnerId = dto.PartnerId;
            promocode.Code = dto.PromoCode;
            promocode.ServiceInfo = dto.ServiceInfo;
           
            promocode.BeginDate = DateTime.Parse(dto.BeginDate);
            promocode.EndDate = DateTime.Parse(dto.EndDate);

            promocode.Preference = preference;
            promocode.PreferenceId = preference.Id;

            promocode.Customers = new List<PromoCodeCustomer>();

            foreach (var item in customers)
            {
                promocode.Customers.Add(new PromoCodeCustomer()
                {

                    CustomerId = item.Id,
                    Customer = item,
                    PromoCodeId = promocode.Id,
                    PromoCode = promocode
                });
            };

            return promocode;
        }
    }
}
