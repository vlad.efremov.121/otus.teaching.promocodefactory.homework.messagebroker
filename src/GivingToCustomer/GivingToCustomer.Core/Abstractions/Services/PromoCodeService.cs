﻿using CommonNamespace;
using GivingToCustomer.Core.Abstractions.Repositories;
using GivingToCustomer.Core.Domain;
using GivingToCustomer.WebHost.Mappers;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GivingToCustomer.Core.Abstractions.Services
{
    public class PromoCodeService : IPromoCodeService
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;
        readonly ILogger<PromoCodeService> _logger;

        public PromoCodeService(IRepository<PromoCode> promoCodesRepository,
                                IRepository<Preference> preferencesRepository,
                                IRepository<Customer> customersRepository,
                                ILogger<PromoCodeService> logger)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
            _logger = logger;
        }
        public async Task<PromoCode> SavePromoCode(GivePromoCodeToCustomerDto dto)
        {
            var preference = await _preferencesRepository.GetByIdAsync(dto.PreferenceId);

            if (preference == null)
            {
                throw new Exception("preference == null");
            }

            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            PromoCode promoCode = PromoCodeMapper.MapFromModel(dto, preference, customers);

            await _promoCodesRepository.AddAsync(promoCode);
            return promoCode;
        }
    }

}
