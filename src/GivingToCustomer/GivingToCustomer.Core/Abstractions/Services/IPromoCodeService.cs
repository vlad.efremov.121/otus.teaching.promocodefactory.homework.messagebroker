﻿using CommonNamespace;
using GivingToCustomer.Core.Domain;
using System.Threading.Tasks;

namespace GivingToCustomer.Core.Abstractions.Services
{
    public interface IPromoCodeService
    {
        Task<PromoCode> SavePromoCode(GivePromoCodeToCustomerDto dto);
    }

}
