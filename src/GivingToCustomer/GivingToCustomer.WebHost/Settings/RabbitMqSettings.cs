﻿namespace GivingToCustomer.WebHost.Settings
{
    public class RabbitMqSettings
    {
        public string HostName { get; set; }
        public string VirtualHost { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool AutomaticRecoveryEnabled { get; set; }
        public int RequestedHeartbeat { get; set; }
        public string QueueName { get; set; }

    }
}
