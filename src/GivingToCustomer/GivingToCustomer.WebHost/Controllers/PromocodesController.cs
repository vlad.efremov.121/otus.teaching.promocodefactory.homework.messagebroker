﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GivingToCustomer.Core.Abstractions.Repositories;
using GivingToCustomer.Core.Domain;
using GivingToCustomer.WebHost.Models;
using GivingToCustomer.Core.Abstractions.Services;
using CommonNamespace;

namespace GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IPromoCodeService _promoCodeService;

        public PromocodesController(IRepository<PromoCode> promoCodesRepository, IPromoCodeService promoCodeService)
        {
            _promoCodesRepository = promoCodesRepository;
            _promoCodeService = promoCodeService;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            try
            {
                var promocodes = await _promoCodesRepository.GetAllAsync();

                var response = promocodes.Select(x => new PromoCodeShortResponse()
                {
                    Id = x.Id,
                    Code = x.Code,
                    BeginDate = x.BeginDate.ToString("yyyy-MM-dd"),
                    EndDate = x.EndDate.ToString("yyyy-MM-dd"),
                    PartnerId = x.PartnerId,
                    ServiceInfo = x.ServiceInfo
                }).ToList();


                return Ok(response);
            }
            catch (Exception ee)
            {
                throw new Exception("Внутрення ошибка сайта", ee);
            }

        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeToCustomerDto request)
        {
            //Получаем предпочтение по имени
            try
            {
                await _promoCodeService.SavePromoCode(request);
                return CreatedAtAction(nameof(GetPromocodesAsync), new { }, null);
            }
            catch (Exception e)
            {
                throw new Exception("Внутрення ошибка сайта", e);
            }
        }
    }
}