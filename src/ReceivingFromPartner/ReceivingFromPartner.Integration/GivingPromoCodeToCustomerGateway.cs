﻿using System.Net.Http;
using System.Threading.Tasks;
using CommonNamespace;
using ReceivingFromPartner.Core.Abstractions.Gateways;
using ReceivingFromPartner.Core.Domain;

namespace ReceivingFromPartner.Integration
{
    public class GivingPromoCodeToCustomerGateway
        : IGivingPromoCodeToCustomerGateway
    {
        private readonly HttpClient _httpClient;

        public GivingPromoCodeToCustomerGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        
        public async Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            var dto = new GivePromoCodeToCustomerDto()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PartnerManagerId = promoCode.PartnerManagerId
            };
            
            var response = await _httpClient.PostAsJsonAsync("api/v1/promocodes", dto);

            response.EnsureSuccessStatusCode();
        }
    }
}