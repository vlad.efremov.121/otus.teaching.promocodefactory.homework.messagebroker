﻿using ReceivingFromPartner.Core.Abstractions.Gateways;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using CommonNamespace;
using ReceivingFromPartner.Core.Domain;
using Microsoft.Extensions.Configuration;
using MassTransit;

namespace ReceivingFromPartner.Integration
{
    public class RabbitMqGateway : IRabbitMqGateway
    {
        private readonly IBusControl _busControl;
        public IConfiguration Configuration { get; }

        public RabbitMqGateway(IBusControl busControl,
                               IConfiguration configuration)
        {
            Configuration = configuration;
            _busControl = busControl;
        }

        public async Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            await _busControl.Publish(new GivePromoCodeToCustomerDto()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                PromoCodeId = promoCode.Id,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerManagerId = promoCode.PartnerManagerId
            }, CancellationToken.None);
        }
    }
}