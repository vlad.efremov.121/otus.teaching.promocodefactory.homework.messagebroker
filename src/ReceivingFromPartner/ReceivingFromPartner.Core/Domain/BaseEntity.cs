﻿using System;

namespace ReceivingFromPartner.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}