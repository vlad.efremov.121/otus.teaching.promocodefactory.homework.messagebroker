﻿using System;
using ReceivingFromPartner.Core.Domain;

namespace ReceivingFromPartner.Core.Domain
{
    public class Preference
        :BaseEntity
    {
        public string Name { get; set; }
    }
}