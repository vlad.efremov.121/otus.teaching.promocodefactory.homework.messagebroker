﻿using ReceivingFromPartner.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ReceivingFromPartner.Core.Abstractions.Gateways
{
    public interface IRabbitMqGateway
    {
        Task GivePromoCodeToCustomer(PromoCode promoCode);
    }

}
