﻿using System;
using System.Threading.Tasks;
using ReceivingFromPartner.Core.Domain;

namespace ReceivingFromPartner.Core.Abstractions.Gateways
{
    public interface IAdministrationGateway
    {
        Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId);
    }
}